<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
	$users = App\User::all()->pluck('id')->toArray();

    return [
    	'user_id' => $faker->randomElement($users),
        'product_id' => $faker->randomNumber(),
        'title' => $faker->sentence(5),
        'url' => $faker->url(),
        'price' => $faker->randomNumber(2),
        'new_price' => $faker->randomNumber(2),
        'image' => 'storage/products/' . str_random(32) . '.jpg',
        'short_description' => $faker->sentence(10),
    ];
});
