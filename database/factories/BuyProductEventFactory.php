<?php

use Faker\Generator as Faker;

$factory->define(App\BuyProductEvent::class, function (Faker $faker) {
	$user = App\User::inRandomOrder()->first();
	$userProduct = $user->products()->inRandomOrder()->first();

    return [
    	'user_id' => $user->id,
    	'product_id' => $userProduct->product_id,
    	'price' => $faker->randomFloat(2, 0.01, 2000),
    	'count' => $faker->numberBetween(1,5),
        'shopper' => str_random(32),
        'shopper_ip' => $faker->randomElement([$faker->ipv4, $faker->ipv6]),
        'shopper_user_agent' => $faker->userAgent(),
        'shopper_referer' => $faker->url(),
        'created_at' => $faker->dateTimeBetween($startDate = '-32 days', $endDate = 'now', $timezone = null)
    ];
});
