<?php

use Faker\Generator as Faker;

$factory->define(App\FrameShow::class, function (Faker $faker) {
	$user = App\User::inRandomOrder()->first();
	$userProduct = $user->products()->inRandomOrder()->first();

    return [
        'user_id' => $user->id,
        'product_id' => $userProduct->product_id,
        'shopper' => str_random(32),
        'shopper_ip' => $faker->randomElement([$faker->ipv4, $faker->ipv6]),
        'shopper_user_agent' => $faker->userAgent(),
        'shopper_referer' => $faker->url(),
        'created_at' => $faker->dateTimeBetween($startDate = '-32 days', $endDate = 'now', $timezone = null)
    ];
});
