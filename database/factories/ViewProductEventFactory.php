<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\ViewProductEvent::class, function (Faker $faker) {
    $user = App\User::inRandomOrder()->first();
    $userProduct = $user->products()->inRandomOrder()->first();

    return [
        'user_id' => $user->id,
        'product_id' => $userProduct->product_id,
        'shopper' => str_random(32),
        'shopper_ip' => $faker->randomElement([$faker->ipv4, $faker->ipv6]),
        'shopper_user_agent' => $faker->userAgent(),
        'shopper_referer' => $faker->url(),
        'created_at' => $faker->dateTimeBetween($startDate = '-32 days', $endDate = 'now', $timezone = null)
    ];
});
