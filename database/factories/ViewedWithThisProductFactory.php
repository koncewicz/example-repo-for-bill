<?php

use Faker\Generator as Faker;

$factory->define(App\ViewedWithThisProduct::class, function (Faker $faker) {
	$user = App\User::inRandomOrder()->first();
	$userProduct = $user->products()->inRandomOrder()->first();
	$userRelatedProduct = $user->products()->inRandomOrder()->first();

    return [
    	'user_id' => $user->id,
        'product_id' => $userProduct->product_id,
        'related_product_id' => $userRelatedProduct->product_id,
        'score' => $faker->randomFloat(18, 0, 100)
    ];
});
