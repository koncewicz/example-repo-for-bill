<?php

use Illuminate\Database\Seeder;

class BuyProductEventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\BuyProductEvent::class, 100)->create();
    }
}
