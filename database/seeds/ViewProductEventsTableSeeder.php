<?php

use Illuminate\Database\Seeder;

class ViewProductEventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ViewProductEvent::class, 50)->create();
    }
}
