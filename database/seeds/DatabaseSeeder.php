<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(ViewedWithThisProductsTableSeeder::class);
        $this->call(ViewProductEventsTableSeeder::class);
        $this->call(FrameShowsTableSeeder::class);
        $this->call(ClickProductEventsTableSeeder::class);
        $this->call(BuyProductEventsTableSeeder::class);
    }
}
