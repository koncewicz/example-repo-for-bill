<?php

use Illuminate\Database\Seeder;

class ClickProductEventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ClickProductEvent::class, 400)->create();
    }
}
