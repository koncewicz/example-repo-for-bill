<?php

use Illuminate\Database\Seeder;

class FrameShowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\FrameShow::class, 1000)->create();
    }
}
