<?php

use Illuminate\Database\Seeder;

class ViewedWithThisProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ViewedWithThisProduct::class, 200)->create();
    }
}
