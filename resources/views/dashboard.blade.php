@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-8">
            <div class="card">
                <div class="card-body">
                    <dashboard-chart></dashboard-chart>
                </div>
            </div>
        </div>
        <div class="col-sm-8" style="margin-top:20px">
            <div class="card">
                <div class="card-body">
                    <purchases-chart></purchases-chart>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
