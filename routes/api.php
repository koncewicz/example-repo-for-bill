<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('V1')->group(function () {
	Route::get('events/view-product', 'Api\Events\ViewProductController@index');
	Route::get('events/buy-product', 'Api\Events\BuyProductController@index');
	Route::get('events/click-on-product-in-frame', 'Api\Events\ClickOnProductInFrameController@index');

	Route::get('frames/viewed-with-this-product', 'Api\Frames\ViewedWithThisProductController@index');
});