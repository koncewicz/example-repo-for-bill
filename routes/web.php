<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'IndexController@welcome');

Route::get('/click', 'ClickController@index')->name('click');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
	Route::get('/dashboard/events-chart', 'DashboardController@eventsChart');
	Route::get('/dashboard/purchases-chart', 'DashboardController@purchasesChart');
});