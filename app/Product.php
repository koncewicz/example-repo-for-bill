<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    protected $visible = ['product_id', 'title', 'price', 'new_price', 'image_url', 'click_url', 'short_description'];
    protected $appends = ['image_url', 'click_url'];

    public function getImageUrlAttribute($value)
    {
        return url($this->image);
    }

    public function getClickUrlAttribute($value)
    {
    	$request = request();

    	return route('click', 
    		[
    			'user_public_id' => $request->user_public_id,
    			'shopper' => $request->shopper,
    			'product_id' => $this->product_id
			]
		);
    }
}
