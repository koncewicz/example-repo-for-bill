<?php

namespace App;

use App\ClickProductEvent;
use Illuminate\Database\Eloquent\Model;

class BuyProductEvent extends Model
{
	public function lastClick()
	{
		return ClickProductEvent::where('user_id', $this->user_id)
			->where('product_id', $this->product_id)
			->where('created_at', '<', $this->created_at)
			->first();
	}
}
