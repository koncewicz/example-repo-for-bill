<?php

namespace App\Http\Controllers\Api\Events;

use App\User;
use Validator;
use App\ViewProductEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ViewProductController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shopper' => 'required|alpha_num|size:32',
            'product_id' => 'required|int',
            'user_public_id' => 'required|exists:users,public_id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('public_id', '=', $request->user_public_id)->first();

        $viewProductEvent = new ViewProductEvent;
        $viewProductEvent->shopper            = $request->shopper;
        $viewProductEvent->product_id         = $request->product_id;
        $viewProductEvent->user_id            = $user->id;
        $viewProductEvent->shopper_ip         = $request->ip();
        $viewProductEvent->shopper_user_agent = $request->header('User-Agent');
        $viewProductEvent->shopper_referer    = $request->header('Referer');
        $viewProductEvent->save();

        return response()->json(['status' => 'success'], 200);
    }
}
