<?php

namespace App\Http\Controllers\Api\Events;

use Validator;
use App\User;
use App\BuyProductEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuyProductController extends Controller
{
    /**
     * @return string
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shopper' 		 => 'required|alpha_num|size:32',
            'product_id'     => 'required|int',
            'count'          => 'required|int',
            'price'          => 'required',
            'user_public_id' => 'required|exists:users,public_id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('public_id', '=', $request->user_public_id)->first();

        $buyProductEvent = new BuyProductEvent;
        $buyProductEvent->user_id            = $user->id;
        $buyProductEvent->shopper            = $request->shopper;
        $buyProductEvent->product_id         = $request->product_id;
        $buyProductEvent->price              = $request->price;
        $buyProductEvent->count              = $request->count;
        $buyProductEvent->shopper_ip         = $request->ip();
        $buyProductEvent->shopper_user_agent = $request->header('User-Agent');
        $buyProductEvent->shopper_referer    = $request->header('Referer');
        $buyProductEvent->save();

        return response()->json(['status' => 'success'], 200);
    }
}
