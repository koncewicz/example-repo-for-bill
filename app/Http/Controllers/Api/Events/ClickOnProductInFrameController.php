<?php

namespace App\Http\Controllers\Api\Events;

use App\User;
use Validator;
use App\Product;
use App\ClickProductEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClickOnProductInFrameController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'shopper' => 'required|alpha_num|size:32',
            'product_id' => 'required|int',
            'user_public_id' => 'required|exists:users,public_id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('public_id', '=', $request->user_public_id)->first();
        $product = Product::where('user_id', $user->id)->where('product_id', $request->product_id)->first();

        if (!$product) {
            return response()->json(['product_id' => ['Product with this product_id and user_public_id not exist.']], 400);
        }

        $clickProductEvent = new ClickProductEvent;
        $clickProductEvent->user_id            = $user->id;
        $clickProductEvent->product_id         = $request->product_id;
        $clickProductEvent->shopper            = $request->shopper;
        $clickProductEvent->shopper_ip         = $request->ip();
        $clickProductEvent->shopper_user_agent = $request->header('User-Agent');
        $clickProductEvent->shopper_referer    = $request->header('Referer');
        $clickProductEvent->save();

        return redirect($product->url, 301);
    }
}
