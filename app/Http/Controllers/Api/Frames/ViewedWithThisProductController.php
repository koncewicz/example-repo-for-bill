<?php

namespace App\Http\Controllers\Api\Frames;

use DB;
use App\View;
use App\User;
use Validator;
use App\Product;
use App\FrameShow;
use Illuminate\Http\Request;
use App\ViewedWithThisProduct;
use App\Http\Controllers\Controller;

class ViewedWithThisProductController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
        	'shopper'    	 => 'required|alpha_num|size:32',
            'product_id'  	 => 'required|int',
            'user_public_id' => 'required|exists:users,public_id'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 400);
        }

        $user = User::where('public_id', '=', $request->user_public_id)->first();
        $product = Product::where('user_id', $user->id)->where('product_id', $request->product_id)->first();

        if (!$product) {
        	return response()->json(['product_id' => ['Product with this product_id and user_public_id not exist.']], 400);
        }

        // Get related product ids
        $ids = ViewedWithThisProduct::where('user_id', $user->id)
            ->where('product_id', $product->product_id)
            ->orderBy('score', 'desc')
            ->pluck('related_product_id')
            ->toArray();

        if (empty($ids)) {
            return response()->json(['status' => 'success', 'products' => []], 200);
        }

        // Get related products
        $orderedIds = implode(', ', $ids);
        $products = Product::whereIn('product_id', $ids)
            ->where('user_id', $user->id)
            ->orderByRaw(DB::raw("FIELD(product_id, $orderedIds)"))
            ->get();

        // Save frame's show to statictics
        $frameShow = new FrameShow;
        $frameShow->user_id            = $user->id;
        $frameShow->shopper            = $request->shopper;
        $frameShow->product_id         = $request->product_id;
        $frameShow->shopper_ip         = $request->ip();
        $frameShow->shopper_user_agent = $request->header('User-Agent');
        $frameShow->shopper_referer    = $request->header('Referer');
        $frameShow->save();

        // Return related products
        return response()->json(['status' => 'success', 'products' => $products], 200);
    }
}
