<?php

namespace App\Http\Controllers;

use DB;
use Carbon\Carbon;
use App\FrameShow;
use Carbon\CarbonPeriod;
use App\BuyProductEvent;
use App\ClickProductEvent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    protected $periodFrom;
    protected $periodTo;

    public function __construct()
    {
        $this->periodFrom = Carbon::now()->subDays(13)->format("Y-m-d");
        $this->periodTo = Carbon::now();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard');
    }

    public function eventsChart()
    {
        $frameShows = FrameShow::select('created_at', DB::raw("(COUNT(*)) as total_views"))
            ->where('user_id', Auth::id())
            ->where('created_at', '>=', $this->periodFrom)
            ->where('created_at', '<', $this->periodTo)
            ->orderBy('created_at', 'asc')
            ->groupBy(DB::raw("DAY(created_at)"))
            ->pluck('total_views', 'created_at');

        $clickProductEvents = ClickProductEvent::select('created_at', DB::raw("(COUNT(*)) as total_clicks"))
            ->where('user_id', Auth::id())
            ->where('created_at', '>=', $this->periodFrom)
            ->where('created_at', '<', $this->periodTo)
            ->orderBy('created_at', 'asc')
            ->groupBy(DB::raw("DAY(created_at)"))
            ->pluck('total_clicks', 'created_at');

        $days = $this->getDaysFromPeriod();
        $frameShows = $this->prepareCollection($frameShows, $days);
        $clickProductEvents = $this->prepareCollection($clickProductEvents, $days);

        $data = [
            'clickProductEvents' => $clickProductEvents,
            'frameShows' => $frameShows,
            'labels' => $days
        ];

        return $data;
    }

    public function purchasesChart()
    {
        $purchases = BuyProductEvent::where('user_id', Auth::id())
            ->where('created_at', '>=', $this->periodFrom)
            ->where('created_at', '<', $this->periodTo)
            ->orderBy('created_at', 'desc')
            ->get();

        foreach ($purchases as $key=>$item) {
            $lastClick = $item->lastClick();
            if (!$lastClick) {
                $purchases->forget($key);
                continue;
            }

            $itemBetween = BuyProductEvent::where('user_id', Auth::id())
                ->where('product_id', $item->product_id)
                ->where('created_at', '>=', $lastClick->created_at)
                ->where('created_at', '<', $item->created_at)
                ->first();

            if ($itemBetween) {
                $purchases->forget($key);
            }
        }

        $days = $this->getDaysFromPeriod();

        if (empty($purchases)) {
            $purchases = $this->prepareCollection([], $days);

            return [
                'purchases' => $purchases,
                'labels' => $days
            ];
        }

        $purchases = BuyProductEvent::select('created_at', DB::raw("(SUM(price * count)) as total_price"))
            ->whereIn('id', $purchases->pluck(['id'])->toArray())
            ->orderBy('created_at', 'asc')
            ->groupBy(DB::raw("DAY(created_at)"))
            ->pluck('total_price', 'created_at');

        $purchases = $this->prepareCollection($purchases, $days);

        foreach ($purchases as $key=>$item) {
            $purchases[$key] = number_format($item, 2, '.', '');
        }

        $data = [
            'purchases' => $purchases,
            'labels' => $days
        ];

        return $data;
    }

    protected function prepareCollection($items, $days)
    {
        $data = [];

        foreach ($items as $key=>$item) {
            $date = (new Carbon($key))->format("Y-m-d");
            $data[$date] = $item;
        }

        foreach ($days as $day) {
            if (!isset($data[$day])) {
                $data[$day] = 0;
            }
        }

        ksort($data);

        return array_values($data);
    }

    protected function getDaysFromPeriod()
    {
        $period = CarbonPeriod::create($this->periodFrom, '1 day', $this->periodTo);

        $days = [];
        foreach ($period as $dt) {
            $days[] = $dt->format("Y-m-d");
        }

        return $days;
    }
}
