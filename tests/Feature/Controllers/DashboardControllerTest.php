<?php

namespace Tests\Feature\Controllers;

use App\User;
use App\Product;
use Carbon\Carbon;
use Tests\TestCase;
use App\BuyProductEvent;
use App\ClickProductEvent;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DashboardControllerTest extends TestCase
{
	use RefreshDatabase;

    public function testPurchasesChart()
    {
    	$user = factory(User::class)->create();

    	$product = factory(Product::class)->create([
    		'user_id' => $user->id
		]);

    	// 11
	    factory(ClickProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'created_at' => Carbon::now()->subDays(4)->format("Y-m-d")
    	]);

	    factory(ClickProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'created_at' => Carbon::now()->subDays(3)->format("Y-m-d")
    	]);

	    $buyProductEvent = factory(BuyProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'price' => 9,
	    	'count' => 2,
	    	'created_at' => Carbon::now()->subDays(2)->format("Y-m-d")
    	]);

	    $buyProductEvent2 = factory(BuyProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'price' => 10,
	    	'count' => 5,
	    	'created_at' => Carbon::now()->subDays(1)->format("Y-m-d")
    	]);

    	//5
	    factory(ClickProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'created_at' => Carbon::now()->subDays(9)->format("Y-m-d")
    	]);

	    $buyProductEvent3 = factory(BuyProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product->product_id,
	    	'count' => 2,
	    	'created_at' => Carbon::now()->subDays(8)->format("Y-m-d")
    	]);

    	$product2 = factory(Product::class)->create([
    		'user_id' => $user->id
		]);

	    factory(ClickProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product2->product_id,
	    	'created_at' => Carbon::now()->subDays(9)->format("Y-m-d")
    	]);

	    $buyProductEvent4 = factory(BuyProductEvent::class)->create([
	    	'user_id' => $user->id,
	    	'product_id' => $product2->product_id,
	    	'created_at' => Carbon::now()->subDays(8)->format("Y-m-d")
    	]);

    	$element5 = number_format((($buyProductEvent3->price * $buyProductEvent3->count) + ($buyProductEvent4->price * $buyProductEvent4->count)), 2, '.', '');
    	$element11 = number_format(($buyProductEvent->price * $buyProductEvent->count), 2, '.', '');

        $response = $this->actingAs($user)
        	->json('GET', '/dashboard/purchases-chart');

        $response
            ->assertStatus(200)
            ->assertJson([
               'purchases' => [
               		5 => $element5,
               		11 => $element11 
           		]
            ]);
    }
}
