<?php

namespace Tests\Feature\Controllers\Api\Events;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BuyProductControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $user = factory(User::class)->create();
        $shopper = str_random(32);

        $response = $this->json('GET', '/api/V1/events/buy-product', [
            'shopper' => $shopper,
            'product_id' => 1,
            'price' => 1122.12,
            'count' => 1,
            'user_public_id' => $user->public_id
        ]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'status' => 'success',
            ]);

        $this->assertDatabaseHas('buy_product_events', [
            'price' => 1122.12,
            'user_id' => $user->id,
            'shopper' => $shopper
        ]);
    }
}
