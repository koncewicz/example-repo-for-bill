<?php

namespace Tests\Feature\Controllers\Api\Events;

use App\User;
use App\Product;
use Tests\TestCase;
use App\ClickProductEvent;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ClickOnProductInFrameController extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create(['user_id' => $user->id]);

        $response = $this->json('GET', '/api/V1/events/click-on-product-in-frame', [
            'shopper' => str_random(32),
            'user_public_id' => $user->public_id,
            'product_id' => $product->product_id
        ])->assertRedirect($product->url);

        $this->assertDatabaseHas('click_product_events', [
            'user_id' => $user->id,
            'product_id' => $product->product_id
        ]);
    }

    public function testIndexCheckProductOwner()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create(['user_id' => $user->id]);

        $response = $this->json('GET', '/api/V1/events/click-on-product-in-frame', [
            'shopper' => str_random(32),
            'user_public_id' => $user->public_id,
            'product_id' => 2
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'product_id' => ['Product with this product_id and user_public_id not exist.'],
            ]);

    }
}
