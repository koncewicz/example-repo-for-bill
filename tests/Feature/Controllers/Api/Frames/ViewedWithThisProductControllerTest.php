<?php

namespace Tests\Feature\Controllers\Api\Frames;

use App\User;
use App\Product;
use Tests\TestCase;
use App\ViewedWithThisProduct;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ViewedWithThisProductControllerTest extends TestCase
{
	use RefreshDatabase;

	public function testIndexRelatedProducts()
	{
        $users = factory(User::class, 3)
        	->create()
        	->each(function ($user) {
        		$user->products()->saveMany(factory(Product::class, 5)->make());
        	});

        $user = $users[0];

        $viewedWithThisProduct1 = factory(ViewedWithThisProduct::class)->create([
        	'user_id' => $user->id,
        	'product_id' => $user->products[0]->product_id,
        	'related_product_id' => $user->products[1]->product_id,
        	'score' => 1
    	]);

        $viewedWithThisProduct2 = factory(ViewedWithThisProduct::class)->create([
        	'user_id' => $user->id,
        	'product_id' => $user->products[0]->product_id,
        	'related_product_id' => $user->products[2]->product_id,
        	'score' => 2
    	]);

    	$shopper = str_random(32);

        $response = $this->json('GET', '/api/V1/frames/viewed-with-this-product', [
            'shopper' => $shopper,
            'user_public_id' => $user->public_id,
            'product_id' => $user->products[0]->product_id
        ]);

        $response
        	->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'products' => [
                	[
                		'product_id' => $user->products[2]->product_id,
                		'price' => $user->products[2]->price
            		],
            		[
                		'product_id' => $user->products[1]->product_id,
                		'price' => $user->products[1]->price
            		]
            	]
            ]);

        $this->assertDatabaseHas('frame_shows', [
            'user_id' => $user->id,
            'shopper' => $shopper
        ]);
	}

	public function testIndexNotFoundRelatedProducts()
	{
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create(['user_id' => $user->id]);

        $response = $this->json('GET', '/api/V1/frames/viewed-with-this-product', [
            'shopper' => str_random(32),
            'user_public_id' => $user->public_id,
            'product_id' => $product->product_id
        ]);

        $response
        	->assertStatus(200)
            ->assertJson([
                'status' => 'success',
                'products' => []
            ]);
	}

    public function testIndexCheckProductOwner()
    {
        $user = factory(User::class)->create();
        $product = factory(product::class)->create(['user_id' => $user->id]);

        $response = $this->json('GET', '/api/V1/frames/viewed-with-this-product', [
            'shopper' => str_random(32),
            'user_public_id' => $user->public_id,
            'product_id' => 2
        ]);

        $response
            ->assertStatus(400)
            ->assertJson([
                'product_id' => ['Product with this product_id and user_public_id not exist.'],
            ]);
    }
}
